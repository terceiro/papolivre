covers = Dir['content/*/cover.svg'].inject([]) do |list,item|
  list << item.sub('.svg', '.png') << item.sub('.svg', '-large.png')
end + Dir['content/*/audio-cover.svg'].inject([]) do |list,item|
  list << item.sub('.svg', '.jpg')
end

def inkscape(*args)
  cmd = ['inkscape'] + args
  puts cmd.join(' ')
  system(*cmd, out: "/dev/null") or fail
end

rule '.png' => ->(f) { f.sub(%r{(-large)?\.png}, '.svg') } do |t|
  params = (t.name =~ /-large.png/) && ['--export-width=720'] || []
  inkscape '--export-type=png', "--export-filename=#{t.name}", *params, t.source
end

rule '.jpg' => ->(f) { f.sub(%r{\.jpg}, '.svg') } do |t|
  inkscape "--export-type=png", "--export-filename=#{t.name}.png", '--export-width=720', '--export-height=720', t.source
  sh 'convert', "#{t.name}.png", t.name
  rm_f "#{t.name}.png"
end

desc 'builds the site'
task :default => covers do
  sh 'nanoc'
end

desc 'adds a new episode'
task :new do
  require 'erubis'
  last = Dir.chdir('content') { Dir['[0-9]*/index.md'] }.map { |d| File.dirname(d).to_i }.sort.last
  episode = last + 1
  new = File.join('content', episode.to_s)
  mkdir_p new
  cp 'util/cover.svg', new
  cp 'util/audio-cover.svg', new
  date = Time.now.strftime('%Y-%m-%d')
  index = File.join(new, 'index.md')
  template = Erubis::Eruby.new(File.read('util/template.erb'))
  File.open(index, 'w') do |f|
    f.write(template.result(date: date, episode: episode))
  end
  puts 'created %s' % index
end

require 'open-uri'
require 'json'
STATS_CACHE = Time.now.strftime('tmp/stats-cache/%Y%m%d.json')
STATS = ENV['STATS'] || 'https://papolivre.org/.well-known/stats/data.json'

file STATS_CACHE do
  mkdir_p File.dirname(STATS_CACHE)
  sh 'curl', '--fail', '--silent', '--output', STATS_CACHE, STATS
end

class Stats < Hash
  def process(entry)
    episode = File.basename(File.dirname(entry['data']))
    format = entry['data'].split('.').last
    if entry['method'] == 'GET'
      self[episode] ||= {}
      self[episode]['total'] ||= 0
      self[episode]['mp3'] ||= 0
      self[episode]['ogg'] ||= 0

      visitors = entry['visitors']['count']
      self[episode]['total'] += visitors
      self[episode][format] += visitors
    end
  end
end

desc 'shows podcast download stats'
task :stats => STATS_CACHE do
  json = open(STATS_CACHE).read
  data = JSON.load(json)

  stats = Stats.new

  data['static_requests']['data'].select { |e|
    e['data'] =~ %r{\.(ogg|mp3)$}
  }.each { |entry|
    stats.process(entry)
    stats
  }

  data['requests']['data'].select { |e|
    e['data'] =~ %r{\.(ogg|mp3)}
  }.each { |entry|
    # some clients will hit the audio files with appended query strings like
    # ?_=1, etc. let's strip those off
    entry['data'].gsub!(%r{(\.(ogg|mp3)\?.*)}, '.\2')
    stats.process(entry)
  }

  puts "Episode total mp3   ogg  "
  puts "------- ----- ----- -----"
  stats.keys.sort_by(&:to_i).each do |episode|
    total = stats[episode]['total']
    mp3 = stats[episode]['mp3']
    ogg = stats[episode]['ogg']
    puts "%-7s %-5d %-5d %-5d" % [episode,total, mp3, ogg]
  end
end

server_port = 8888
desc 'Runs a local server'
task :server do
  puts "Server available at http://localhost:#{server_port}"
  puts "Hit Control-C to stop"
  puts
  ruby '-run', '-e', 'httpd', 'output/', '-p', server_port.to_s
end

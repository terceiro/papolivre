# Papo livre - manutenção do site

## Como construir o site localmente

Requisitos:

* Ruby
* nanoc 4 (apt-get install nanoc no debian 9+, ou via rubygems)
* inkscape

Pra construir o site basta:

```
$ rake
```

Os arquivos resultantes estarão em `output/`.

Pra servir o site localmente você pode usar por exemplo:

```
$ rake server
```

Você pode acessar a sua versão local do site acessando http://localhost:8888/
no seu navegador.

## Checklist para lançamento de um novo episódio

Veja checklist-episodio.otl

## Checklist para edição de episódio

* fazer o corte inicial, removendo erros e quebrando as faixas nos blocos do
  episódio
* efeitos:
  * redução de ruído
  * remoção de picos
  * compressor
* trilha sonora
  * apenas licenças compatíveis com CC BY-SA
  * tomar nota dos clipes usados para incluir nos créditos
* marcar blocos com rótulos pra virarem capítulos

### Sites onde baixar trilhas sonoras e efeitos

* https://zero-project.gr/    MUSIC
* https://freesound.org/      SFX
* http://www.freesfx.co.uk/   SFX, MUSIC
* https://www.soundjay.com/   SFX, MUSIC

---
kind: article
audio:
- filename: papolivre-11-administracao-de-sistemas.mp3
  filesize: 73906663
- filename: papolivre-11-administracao-de-sistemas.ogg
  filesize: 46335116
created_at: "2017-11-07"
title: "#11 - Administração de sistemas"
duration: 3621
links: |
  * [Daniel Lenharo](http://blog.lenharo.eti.br/)
  * ["Sendo SysAdmin com Software Livre"](http://www.lenharo.eti.br/Arq-palestras/SysAdminSL.pdf) (slides em PDF)
  * [CELEPAR](http://www.celepar.pr.gov.br/)
  * [iptables](http://www.netfilter.org/projects/iptables/)
  * [fwbuilder](https://github.com/fwbuilder/fwbuilder/)
  * [OSSIM](https://www.alienvault.com/products/ossim)
  * [OpenLDAP](http://www.openldap.org/)
  * [SAMBA](https://www.samba.org/)
  * [SQUID](https://www.samba.org/)
  * [SARG](https://sourceforge.net/projects/sarg/)
  * [Graylog](https://www.graylog.org/)
  * [Grafana](http://grafana.org)
  * [Bacula](http://www.bacula.org/)
  * [backuppc](http://backuppc.sourceforge.net/)
  * [rsync](https://rsync.samba.org/)
  * [rdiff-backup](http://rdiff-backup.nongnu.org/)
  * [duplicity](http://duplicity.nongnu.org/)
  * [obnam](http://obnam.org/)
  * [DigitalOcean](https://www.digitalocean.com/)
  * [Linode](https://www.linode.com/)
  * [SSH](https://en.wikipedia.org/wiki/Secure_Shell)
  * [Port knocking](https://en.wikipedia.org/wiki/Port_knocking)
  * [ansible](https://www.ansible.com/)
  * [Chef](https://www.chef.io/)
  * [Monitorix](http://www.monitorix.org/)
  * [Zabbix](https://www.zabbix.com/)
  * [Devops](https://en.wikipedia.org/wiki/DevOps)
  * Dicas
    * [Papo de sysadmin](http://papodesysadmin.org/) (Lenharo)
    * [Multicloud](http://multicloudbrasilday.com.br/) (Lenharo)
    * [Gracie Barra](http://graciebarra.com.br/) (Lenharo)
    * [Paraná Clube](http://paranaclube.com.br/) (Lenharo)
    * [Vida de Suporte](http://vidadesuporte.com.br/) (Terceiro)
    * [chake](https://gitlab.com/terceiro/chake) (Terceiro)
    * [acesso.me](http://acesso.me) (Thiago)
    * [openRA](http://www.openra.net/) (Thiago)
    * [CPWeekend PATO BRANCO](http://brasil.campus-party.org/cpweekend-pato-branco/) (Paulo)
    * [RubyConf](http://rubyconf.com.br) (Paulo)
    * [Fórum Goiano de Software Livre](http://2017.fgsl.net/) (Paulo)
    * [Drupalcamp São Paulo](http://saopaulo.drupalcamp.com.br/) (Paulo)
    * [PHP Conference Brasil](http://www.phpconference.com.br) (Paulo)
short_description: |
  Conversamos com Daniel Lenharo, sysadmin e Desenvolvedor Debian, sobre a vida
  de um administrador de sistemas, as diferentes subáreas de atuação,
  ferramentas livres relevantes na área, e boas práticas na manutenção de
  sistemas.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça conversam com o convidado
Daniel Lenharo sobre a área da administração de sistemas. Assuntos abordados
incluem as diferentes subáreas da administração de sistemas (segurança, gestão
de usuários, gestão de acesso, autenticação, monitoramento, backup, etc), as
principais ferramentas livres utilizadas por profissionais da área, melhores
práticas para gestão de sistemas, e o conceito de DevOps.


Edição: Thiago Mendonça

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).


---
kind: article
audio:
- filename: papolivre-15-retrospectiva-2017.mp3
  filesize: 33153359
- filename: papolivre-15-retrospectiva-2017.ogg
  filesize: 30984224
created_at: "2018-01-26"
title: "#15 - Retrospectiva 2017"
duration: 1709
links: |
  * 31/01 a 06/02: Campus Party Brasil 2017 - CPBR10
    * [Karen Sandler: Cyborgs unite!](https://www.youtube.com/watch?v=55EHyeCL1RI)
  * 17-19/03: [MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/)
  * 05/04: [Canonical abandona o desenvolvimento do Unity e o projeto de convergência](https://insights.ubuntu.com/2017/04/05/growing-ubuntu-for-cloud-and-iot-rather-than-phone-and-convergence/)
  * 08/04: [FLISOL](https://flisol.info/FLISOL2017/Brasil)
  * 22/05: lançamento do [episódio 0 do Papo Livre](../0/)
  * 02/06: [Stallman em Curitiba](http://rms.curitibalivre.org.br/)
  * 17/06: Lançamento do Debian 9. Ver [episódio #2 - Debian 9 (Stretch)](../2/)
  * 28/06: [FSF dá o selo RYF para 15 novos equipamentos da Technoethical](https://www.fsf.org/news/fifteen-new-devices-from-technoethical-now-fsf-certified-to-respect-your-freedom)
  * 5-8/07: [FISL adiado para 2018](http://softwarelivre.org/fisl18/blog/fisl18-esta-adiado-para-julho-de-2018)
  * 6-12/08: Debconf17. Veja [#6 - Debconf e contribuindo com o Debian](../6/)
  * 13/09: lançado GNOME 3.26. Veja [#10 - GNOME](../10/)
  * 9/10: [Purism Librem 5 100% financiado em 1.5 milhões de dólares](https://puri.sm/posts/librem-5-phone-funding-target-met-one-week-after-50percent-mark/)
  * 01/11: [Gitlab muda política de contribuições](https://about.gitlab.com/2017/11/01/gitlab-switches-to-dco-license/)
  * 01/11: [Canonical se junta ao conselho consultivo da GNOME Foundation](https://insights.ubuntu.com/2017/11/01/canonical-joins-gnome-foundation-advisory-board/)
  * 12/11: [lançado Linux 4.14, com árvore de firmware removida](https://kernelnewbies.org/Linux_4.14)
  * 21/12: [PureOS aprovado pela FSF na lista de distribuições recomendadas](https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1)
  * Eventos no primeiro semestre de 2018
    * [Campus Party Brasil 2018 30/01 a 04/02 - São Paulo](http://brasil.campus-party.org)
    * [Python Sul 2018 - 06 a 08 de abril](http://pythonsul.org)
    * [MiniDebConf Curitiba 2018 - 11 a 14 de abril](https://minidebconf.curitiba.br)
    * [FLISOL - 28/04](http://flisol.info)
    * [CryptoRave - 04 a 05 de maio](https://cryptorave.org)
  * Bônus: episódios favotiros de 2017
    * [#12](../12/) e [#13](../13/): Promovendo Software Livre 1 e 2 (Thiago)
    * [#9 - Linux Kernel Hacking](../9/) (Paulo)
    * [#14 - GoboLinux](../14/) (Terceiro)
short_description: |
  Após um intervalo nos episódios na virada de ano, estamos de volta com
  uma discussão sobre eventos importantes para a comunidade software livre, sob
  a visão particular do Papo Livre.
---

Na virada do ano a gente deu um intervalo na gravação e na publicação dos
episódios do Podcast, mas agora estamos de volta! No retorno do Papo Livre,
Antonio Terceiro, Paulo Santana e Thiago Mendonça fazem uma retrospectiva 2017.
A discussão envolve acontecimentos importantes para a comunidade software
livre, em especial em tópicos nos quais a gente presta mais atenção, e tópicos
que foram abordados no Papo Livre.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).


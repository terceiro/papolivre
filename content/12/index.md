---
kind: article
audio:
- filename: papolivre-12-promovendo-o-software-livre.mp3
  filesize: 73272754
- filename: papolivre-12-promovendo-o-software-livre.ogg
  filesize: 69517196
created_at: "2017-11-21"
title: "#12 - Promovendo o Software Livre"
duration: 4133
links: |
  * [Alexandre Oliva](http://www.fsfla.org/~lxoliva/)
  * [SecureDrop and Alexandre Oliva are 2016 Free Software Awards winners](http://www.fsf.org/news/securedrop-and-alexandre-oliva-are-2016-free-software-awards-winners)
  * [AntiCast 307 – Religiões de Paródia](http://anticast.com.br/2017/10/anticast/anticast-307-religioes-de-parodia/)
  * [Pastafarianismo/Igreja do Monstro de Spagheti Voador](https://www.venganza.org/) (ou na [Wikiedia](https://en.wikipedia.org/wiki/Flying_Spaghetti_Monster))
  * [Copyleft](https://www.gnu.org/licenses/copyleft.pt-br.html)
  * [NerdTech 21 — Software livre](https://jovemnerd.com.br/?podcast=software-livre)
  * [Papo Livre #1 — Meios de Comunicação](/1/)
short_description: |
 Conversamos com Alexandre Oliva sobre como como promover o software livre
 dentro e fora da área de Computação, desafios do movimento software livre, e
 contradições com o movimento open source. Parte 1 de 2.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça conversam com o convidado
Alexandre Oliva sobre como falar sobre software livre para pessoas fora e
dentro da área da Computação, a necessidade de colaborações de
não-programadores a projetos de software livre, desafios a serem vencidos em
levar a reflexão sobre liberdade do usuário à sociedade, e contradições entre
as comunidades software livre e _open source_.

Este episódio é a primeira parte da conversa, a segunda parte está no
[episódio #13](../13/).

Edição: Antonio Terceiro

Imagem de capa: [Road](https://www.flickr.com/photos/janitors/15795816662/),
por [Kārlis Dambrāns](https://www.flickr.com/photos/janitors/). Licença
Creative Commons Attribution (CC-BY).

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

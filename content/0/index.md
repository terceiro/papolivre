---
kind: article
audio:
- filename: papolivre-0-rms-no-brasil-conceitos-sl.mp3
  filesize: 65844159
- filename: papolivre-0-rms-no-brasil-conceitos-sl.ogg
  filesize: 55780421
created_at: "2017-05-22"
title: "#0 - Piloto: RMS no Brasil e conceitos básicos de Software Livre"
duration: 4110.985556
links: |
  * [Curitiba](https://en.wikipedia.org/wiki/Curitiba)
  * [Debian](https://www.debian.org/)
  * [Departamento de Informática - UFPR](http://www.inf.ufpr.br/dinf/)
  * [Conceição de Macabu](https://en.wikipedia.org/wiki/Concei%C3%A7%C3%A3o_de_Macabu)
  * [Salvador](https://en.wikipedia.org/wiki/Salvador,_Bahia)
  * [Linaro](https://www.linaro.org/)
  * Outros podcasts
    * [Braincast](http://www.b9.com.br/podcasts/braincast/)
    * [Politicast](http://politicast.info/)
    * [Hackincast](https://hackncast.org/)
    * [Castalio podcast](http://castalio.info/)
    * [Databasecast](http://databasecast.com.br/)
    * [Diocast](http://www.diolinux.com.br/search/label/DioCast)
    * [Floss Weekly](https://twit.tv/shows/floss-weekly)
    * [Opencast](http://tecnologiaaberta.com.br/category/opencast/)
    * [Changelog](https://changelog.com/podcast)
    * [Free as in Freedom](http://faif.us/)
    * [Anticast](http://anticast.com.br/podcast/anticast/)
    * [Mamilos](http://www.b9.com.br/podcasts/mamilos/)
    * [Nerdcast](https://jovemnerd.com.br/nerdcast/)
    * [Decrépitos](http://decrepitos.com/)
  * [G1: Campus Party Brasília terá palestra com idealizador do software livre](http://g1.globo.com/distrito-federal/noticia/campus-party-brasilia-tera-palestra-com-idealizador-do-software-livre.ghtml)
  * [Richard Stallman's Personal Site'](https://stallman.org/)
  * [Stallman Tour Brasil 2017](http://softwarelivre.org/portal/noticias/stallman-tour-brasil-2017)
  * [Karen Sandler na campus party 2017](https://sfconservancy.org/news/2017/feb/09/karen-campus-party-brasil/)
  * [Projeto GNU](https://www.gnu.org/)
  * [GCC - GNU Compiler Collection](https://gcc.gnu.org/)
  * [Emacs](https://www.gnu.org/s/emacs/)
  * [Wikipedia: History of free and open-source software](https://en.wikipedia.org/wiki/History_of_free_and_open-source_software)
  * Episódios do Politicast sobre software livre:
    * [Politicast #51 – Software Livre (1 de 3)](http://politicast.info/2016/08/07/politicast-51-software-livre-1-de-3/)
    * [Politicast #52 – Software Livre (2 de 3)](http://politicast.info/2016/08/14/politicast-52-software-livre-2-de-3/)
    * [Politicast #53 – Software Livre (3 de 3)](http://politicast.info/2016/08/21/politicast-53-software-livre-3-de-3/)
  * [Redhat](https://www.redhat.com/)
  * Karl Foggel no Request for Commits:
    * [Open Source, Then and Now (Part 1) with Karl Fogel](https://changelog.com/rfc/1)
    * [Open Source, Then and Now (Part 2) with Karl Fogel](https://changelog.com/rfc/2)
  * [Owncloud](https://owncloud.org/) (e seu fork, [Nextcloud](https://nextcloud.com/))
  * [Colivre](http://colivre.coop.br/)
  * [Stoq](https://www.stoq.com.br/)
  * [Porque vender software livre é mais difícil do que parece](https://www.youtube.com/watch?v=mcUuiL3gAb8).
    Palestra do Christian Reis na Campus Party Brasil 7 (2014)
  * [License GPL - GNU General Public License](https://www.gnu.org/licenses/gpl.html)
  * [Licença Affero GPL](https://www.gnu.org/licenses/agpl.html)
  * [The JavaScript Trap](https://www.gnu.org/philosophy/javascript-trap.html)
  * [Noosfero](http://noosfero.org/)
  * [softwarelivre.org](http://softwarelivre.org/)
  * [Campus Party Brasil](http://brasil.campus-party.org/)
  * [FISL17 (2016)](http://softwarelivre.org/fisl17/)
  * [X60S](http://www.thinkwiki.org/wiki/Category:X60s)
  * [Libreboot](https://libreboot.org/)
  * [Libreoffice](https://www.libreoffice.org/)
  * [Intel's remote AMT vulnerablity](http://mjg59.dreamwidth.org/48429.html)
short_description: |
  Neste primeiro episódio, conversamos sobre a próximo vinda ao Brasil de
  Richard Mattew Stallman (RMS), concepção e expectativas para este novo
  podcast, conceitos básicos de software livre, e possíveis temas para
  episódios futuros
---
Neste primeiro episódio, Antonio Terceiro, Paulo Santana e Thiago Mendonça
discutem a próximo vinda ao Brasil de Richard Mattew Stallman (RMS), o fundador
do movimento do Software Livre. RMS vai fazer uma série de palestras durante um
período de 3 semanas a partir do final de maio de 2017. Em seguida a conversa
abrange diversos temas, como concepção e expectativas para este novo podcast,
origens do software livre, conceitos básicos de software livre, e possíveis
temas para episódios futuros

Edição: Thiago Mendonça

Música: [Tanzen (CM+T
02)](http://freemusicarchive.org/music/KieLoBot/Chicky__Chacky/Tanzen_CMT_02),
por [KieLoBot](http://freemusicarchive.org/music/KieLoBot/). Licenciada sob a
licença [Creative Commons Attribution-NonCommercial-NoDerivatives
4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/).

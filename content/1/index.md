---
kind: article
audio:
- filename: papolivre-1-meios-de-comunicacao.mp3
  filesize: 97907915
- filename: papolivre-1-meios-de-comunicacao.ogg
  filesize: 77736621
created_at: "2017-06-06"
title: "#1 - meios de comunicação"
duration: 7601.037438
links: |
  * [Free Software Needs Free Tools](https://mako.cc/writing/hill-free_tools.html)
  * [Rocket.chat](https://rocket.chat/)
  * [IRC - Internet Relay Chat (Wikipedia)](https://en.wikipedia.org/wiki/Internet_Relay_Chat)
  * [IRCv3](http://ircv3.net/)
  * [Apache Software Foundation - how it works, Communication](https://www.apache.org/foundation/how-it-works.html#communication)
  * [Mailing list (Wikipedia)](https://en.wikipedia.org/wiki/Mailing_list)
  * [Netiqueta - Wikipedia em Português](https://pt.wikipedia.org/wiki/Netiqueta)
  * [Eudora (email client)](https://en.wikipedia.org/wiki/Eudora_%28email_client%29)
  * [Sandstorm](https://sandstorm.io/)
  * [Mailman, the GNU Mailing List Manager](https://www.gnu.org/software/mailman/)
  * [feed2imap](https://github.com/feed2imap/feed2imap)
  * [lista debian-user-portuguese](https://lists.debian.org/debian-devel-portuguese/)
  * [Telegram](https://telegram.org/)
  * [Telegram FAQ](https://telegram.org/faq)
  * [Is Telegram secure? (StackExchange Security)](https://security.stackexchange.com/questions/49782/is-telegram-secure)
  * [Matrix](http://matrix.org/)
  * [Matrix’s ‘Olm’ End-to-end Encryption security assessment released – and implemented cross-platform on Riot at last!](https://matrix.org/blog/2016/11/21/matrixs-olm-end-to-end-encryption-security-assessment-released-and-implemented-cross-platform-on-riot-at-last/#comment-25866)
  * [Riot](https://about.riot.im/)
  * [diaspora*](https://diasporafoundation.org/)
  * [Framasoft](https://framasoft.org/)
  * [XMPP](https://xmpp.org/)
  * [Jabber-BR](https://jabber-br.org/)
  * [GNU Ring](https://ring.cx/)
short_description: |
  Conversamos sobre os diversos meios de comunicação utilizados peloa
  comunidadade de software livre. Falamos IRC, listas de discussão, Telegram, e
  Matrix/Riot, e outros.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça discutem os diversos meios de
comunicação em comunidades de software livre. A discussão começa pelos meios
mais "antigos", como IRC e listas de discussão e chega aos mais "modernos",
passando pelo meio livre e meio proprietário Telegram, e chega à mais nova
promessa  nessa área, Matrix (e o seu cliente mais famoso/viável, Riot).

**Erratas (em áudio na introdução do episódio [#2](/2/)):**

* É dito que o Signal não é livre, e isso não é verdade. O cliente Signal é
  licenciado sob GPLv3, e o servidor sob AGPLv3. No entanto, o Signal depende
  por padrão do
  [Google Cloud Messaging](https://en.wikipedia.org/wiki/Google_Cloud_Messaging)
  (GCM), que não é livre. Em fevereiro de 2017, foi adicionado
  [suporte a funcionar sem GCM](https://github.com/WhisperSystems/Signal-Android/commit/1669731329bcc32c84e33035a67a2fc22444c24b),
  mas no momento em que esta errata está sendo escrita ainda não existe um
  build do Signal sem GCM no F-Droid por exemplo.
* O [Riot.im](https://about.riot.im/) para Android tem um problema parecido com
  o Signal, e o build disponível no Google Play vem com GCM. No entanto, o
  build disponível no F-Droid é totalmente livre e vem sem GCM.
* É dito que é impossível apagar uma conta Facebook, mas na verdade é. O
  Facebook diz explicitamente que os seus dados nunca vão ser removidos de
  fato, mas o perfil deixa de ser visível para os outros usuários.

Edição: Thiago Mendonça.

Trilha sonora: [Burnin Star (2016)](https://www.jamendo.com/track/1378740/burnin-star),
por [StrangeZero](https://www.jamendo.com/artist/3799/strangezero).
Licenciada sob a licença [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/).

---
kind: article
audio:
- filename: papolivre-19-debconf18.mp3
  filesize: 70892735
- filename: papolivre-19-debconf18.ogg
  filesize: 55587090
created_at: "2018-09-05"
title: "#19 - Debconf18 em Hsinchu, Taiwan"
duration: 3672
links: |
  * [Debconf18](https://debconf18.debconf.org/)
  * [Debconf19 EM CURITIBA](https://debconf19.debconf.org/)
short_description: |
  Paulo Santana,
  Daniel Lenharo,
  Helen Koike,
  Rodrigo Siqueira,
  Lucas Kanashiro,
  Arthur del Esposte
  e
  Samuel Henrique
  participam de uma conversa gravada na Debconf18, em Hsinchu, Taiwan, sobre a
  experiência de participar da conferência anual do projeto Debian.
---

Uma turminha do barulho arranjando altas confusões do outro lado do mundo.

Paulo Santana,
Daniel Lenharo,
Helen Koike,
Rodrigo Siqueira,
Lucas Kanashiro,
Arthur del Esposte
e
Samuel Henrique
participam de uma conversa gravada na Debconf18, em Hsinchu, Taiwan, sobre
a experiência de participar da conferência anual do projeto Debian.

Edição: Rodrigo Siqueira

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

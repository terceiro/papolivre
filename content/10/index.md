---
kind: article
audio:
- filename: papolivre-10-gnome.mp3
  filesize: 82404803
- filename: papolivre-10-gnome.ogg
  filesize: 76621670
created_at: "2017-10-21"
title: "#10 - GNOME"
duration: 4432
links: |
  - [GNOME](https://www.gnome.org/)
  - [Georges Stavracas - website](https://feaneron.com/)
  - [GNOME Calendar](https://wiki.gnome.org/Apps/Calendar)
  - [GNOME Tasks](https://wiki.gnome.org/Apps/Todo)
  - GNOME Settings
  - [Endless](https://endlessos.com/)
  - [GNOME 3.26 released](https://www.gnome.org/news/2017/09/gnome-3-26-released/)
  - [Release Party — São Paulo](https://feaneron.com/2017/09/05/release-party-in-sao-paulo-brazil/)
  - As diferentes partes do GNOME
    - Core
      - [GTK+](http://www.gtk.org/)
      - [Clutter](https://wiki.gnome.org/Projects/Clutter)
      - [Cogl](https://www.cogl3d.org/)
      - [Gjs](https://wiki.gnome.org/Projects/Gjs)
      - [GNOME Shell](https://wiki.gnome.org/Projects/GnomeShell)
    - Aplicativos
      - [Calendário](https://wiki.gnome.org/Apps/Calendar)
      - [Tarefas](https://wiki.gnome.org/Apps/Todo)
      - [Builder](https://wiki.gnome.org/Apps/Builder)
  - [Flatpak](http://flatpak.org/)
  - [Snap](https://snapcraft.io/)
  - [OSTree](https://ostree.readthedocs.io/en/latest/)
  - [GNOME Newcomers](https://wiki.gnome.org/Newcomers/) (Georges)
  - [GObject Reference Manual](https://developer.gnome.org/gobject/stable/)
  - [GObject Introspection Reference Manual](https://developer.gnome.org/gi/stable/)
  - [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox)
  - [Mutter](https://en.wikipedia.org/wiki/Mutter_(software))
  - Tradução
    - [l10n.gnome.org](https://l10n.ggnome.org/)
    - [lista de tradução]()
  - [GNOME Extensions](https://extensions.gnome.org/)
  - Extensões favoritas
    - [Pomodoro](http://gnomepomodoro.org/) (Georges)
    - [Timezone](https://extensions.gnome.org/extension/1060/timezone/) (Georges)
    - [AlternateTab](https://extensions.gnome.org/extension/15/alternatetab/) (Terceiro)
    - [Auto Move Windows](https://extensions.gnome.org/extension/16/auto-move-windows/) (Terceiro)
    - [Workspace Indicator](https://extensions.gnome.org/extension/21/workspace-indicator/) (Terceiro)
    - [Caffeine](https://extensions.gnome.org/extension/517/caffeine/) (Thiago)
    - [EasyScreenCast](https://extensions.gnome.org/extension/690/easyscreencast/) (Thiago)
    - [No Topleft Hot Corner](https://extensions.gnome.org/extension/118/no-topleft-hot-corner/) (Thiago)
    - [Screenshot tool](https://extensions.gnome.org/extension/1112/screenshot-tool/) (Thiago)
  - [GNOME Tweaks](https://wiki.gnome.org/Apps/GnomeTweakTool)
  - GNOME Settings
  - [Fundação GNOME](https://www.gnome.org/foundation/)
  - [GUADEC](https://2017.guadec.org/)
  - [GNOME.Asia](https://2017.gnome.asia/)
  - [Libre Application Summit by GNOME](https://las.gnome.org/)
  - Dicas
    - [GNOME GitLab](https://gitlab.gnome.org/) (Georges)
    - IRC GNOME Brasil - `#gnome-br` em `irc.gnome.org` (Georges)
    - [GNOME Newcomers](https://wiki.gnome.org/Newcomers/) (Georges)
    - [Celular Librem5](https://puri.sm/shop/librem-5/) (Georges)
    - [Celular OnePlus](https://oneplus.net/) (Thiago)
    - [FontAwesome](https://fontawesome.io) (Terceiro)
    - Provedores de serviços independentes (Terceiro)
      - [RiseUp](https://riseup.net/)
      - [Disroot](https://disroot.org/)
      - [Framasoft](https://framasoft.org/)
    - [Outreachy](https://www.outreachy.org/) (Paulo)
    - [Livro "O que aprendi sendo xingado na internet"](https://www.saraiva.com.br/o-que-aprendi-sendo-xingado-na-internet-9341102.html), de Leonardo Sakamoto (Paulo)
    - [Como foi a vinda do Richard Stallman para Curitiba em 2017 - parte 1](http://phls.com.br/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-1)
short_description: |
  Conversamos com Georges Stavracas (a.k.a feaneron), desenvolvedor do GNOME,
  sobre conceitos básicos sobre o GNOME, as suas tecnologias, o processo de
  desenvolvimento, como contribuir, e nossas extensões preferidas.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça conversam com Georges
Stavracas (a.k.a feaneron) sobre o projeto GNOME. Assuntos: o que é o GNOME, o
laçamento do GNOME 3.26, como o Georges começou a contribuir com o projeto,
dicas para quem gostaria de começar a contribuir, tecnologias utilizadas no
GNOME, como funciona o processo de desenvolvimento do GNOME, extensões do GNOME
Shell, GNOME em eventos e eventos do GNOME.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

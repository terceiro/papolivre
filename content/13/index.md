---
kind: article
audio:
- filename: papolivre-13-promovendo-o-software-livre-2.mp3
  filesize: 57479810
- filename: papolivre-13-promovendo-o-software-livre-2.ogg
  filesize: 54426276
created_at: "2017-12-05"
title: "#13 - Promovendo o Software Livre 2"
duration: 3083
links: |
  * [Linux Developer Conference Brazil](http://linuxdev-br.net/)
  * [LibrePlanet](https://libreplanet.org/2018/)
  * [Software Freedom Day](https://softwarefreedomday.org/)
  * [FLISOL](https://flisol.info/)
  * [Fórum Tecnológico de Software Livre](http://ftsl.org.br/)
  * [Scuttlebot](https://scuttlebot.io/)
  * [git-ssb](https://github.com/clehner/git-ssb/)
  * [GNU Linux-Libre](https://linux-libre.fsfla.org/)
  * [Linux 4.14](https://kernelnewbies.org/Linux_4.14)
  * [Diretrizes para Distribuições de Sistemas Livres (GNU FSDG)](https://www.gnu.org/distros/free-system-distribution-guidelines.pt-br.html)
  * Dicas
    * [Scuttlebot](https://scuttlebot.io/) (Oliva)
    * [git-ssb](https://github.com/clehner/git-ssb/) (Oliva)
    * [twister](http://twister.net.co/) (Oliva)
    * [Replicant](https://www.replicant.us/) (Oliva)
    * [linux-libre-firmware](https://jxself.org/firmware/) (Oliva)
    * [Workshop de raspagem de dados com Python em Curitiba](https://mulheres.eti.br/workshop-raspagem-dados-python-curitiba/) (Paulo)
    * [Texto traduzido do Stallman sobre Hackathons](https://www.gnu.org/philosophy/hackathons.pt-br.html) (Paulo)
    * [MiniDebconf Curitiba 2018: 11 a 14 de abril](https://minidebconf.curitiba.br/) (Paulo)
    * [ZeroNet](https://zeronet.io/) (Paulo)
    * [Tilix](https://gnunn1.github.io/tilix-web/) (Thiago)
    * Fazer exercicio (Thiago)
    * Fazer atividades offline (Thiago)
    * [Seja um apoiador da Free Software Foundation](https://fsf.org/appeal) (Terceiro)
    * [Seja um apoiador da Software Freedom Conservancy](https://sfconservancy.org/supporter/) (Terceiro)
    * [Vida de Programador](http://vidadeprogramador.com.br/) (Terceiro)
    * [Penny Dreadful](https://en.wikipedia.org/wiki/Penny_Dreadful_%28TV_series%29) (Terceiro)
short_description: |
  Conversamos com Alexandre Oliva sobre eventos de software livre e software
  livre em eventos, desafios de comunicação para colaboração em projetos de
  software livre, e sobre o passado, presente e futuro do GNU Linux-libre. Parte
  2 de 2.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça conversam com o convidado
Alexandre Oliva sobre como promover o software livre em eventos, a necessidade
de termos eventos organizados por pessoas alinhadas com os objetivos do
software livre, os desafios de comunicação no mundo contemporâneo, e sobre o
projeto GNU Linux-libre.

Este episódio é a segunda parte da conversa que começou no
[episódio #12](../12/).

Edição:

[Imagem de capa](https://www.flickr.com/photos/gential/38655012416/)
por [Gential LAMBERT](https://www.flickr.com/photos/gential/).
Licença Creative Commons Attribution (CC-BY).

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

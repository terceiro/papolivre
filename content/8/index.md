---
kind: article
audio:
- filename: papolivre-8-gnu-linux-em-dispositivos-moveis.mp3
  filesize: 106854400
- filename: papolivre-8-gnu-linux-em-dispositivos-moveis.ogg
  filesize: 93359072
created_at: "2017-09-23"
title: "#8 - GNU/Linux em dispositivos móveis"
duration: 6672
links: |
  * [Thadeu Cascardo](https://cascardo.eti.br/)
  * [Posts no blog do Cascardo sobre GNU/Linux em dispositos móveis](https://cascardo.eti.br/tags/mobile/)
  * [arquitetura do sistema operacional móvel, ou o que precisa ser feito](https://cascardo.eti.br/blog/GNU_on_Smartphones_part_II/)
  * [Fastboot](https://en.wikipedia.org/wiki/Android_software_development#Fastboot)
  * [Heimdall](http://glassechidna.com.au/heimdall/)
  * [systemd](https://www.freedesktop.org/wiki/Software/systemd/)
  * [System V](https://en.wikipedia.org/wiki/UNIX_System_V)
  * [postmarketOS](https://postmarketos.org/)
  * [musl libc](https://www.musl-libc.org/)
  * [GNU libc](https://www.gnu.org/software/libc/)
  * [Purism Librem 5](https://puri.sm/shop/librem-5/)
  * [Openmoko](https://en.wikipedia.org/wiki/Openmoko)
  * [Ubuntu Touch a.k.a. Ubuntu Phone](https://en.wikipedia.org/wiki/Ubuntu_Touch)
  * [Plasma Mobile](https://plasma-mobile.org/)
  * [Halium](https://halium.org/)
  * [Freedesktop](https://www.freedesktop.org/wiki/)
  * [Bionic libc](https://en.wikipedia.org/wiki/Bionic\_(software))
  * [libhybris](https://github.com/libhybris/libhybris)
  * [OpenEmbedded](https://www.openembedded.org/wiki/Main\_Page)
  * [GPE Palmtop Environment](https://en.wikipedia.org/wiki/GPE_Palmtop_Environment)
  * [GNOME](https://wiki.gnome.org/Projects/GnomeShell)
  * [Wayland](https://wayland.freedesktop.org/)
  * [DRM - Direct Rendering Manager](https://en.wikipedia.org/wiki/Direct_Rendering_Manager)
  * [pidgin e libpurple](http://pidgin.im/)
  * [Telepathy](https://telepathy.freedesktop.org/)
  * [OsmAnd](http://osmand.net/)
  * [Shashlik](http://www.shashlik.io/)
  * [Anbox](https://anbox.io/)
  * [F-droid](https://f-droid.org/)
  * Histórico de iniciativas
    * [μClinux](https://en.wikipedia.org/wiki/uClinux)
    * [busybox](https://busybox.net/)
    * [Familiar Linux](https://en.wikipedia.org/wiki/Familiar_Linux)
    * [GPE](https://en.wikipedia.org/wiki/GPE_Palmtop_Environment)
    * [OPIE](https://en.wikipedia.org/wiki/Open_Palmtop_Integrated_Environment)
    * [OpenEZX](https://en.wikipedia.org/wiki/OpenEZX)
    * [Openmoko](https://en.wikipedia.org/wiki/Openmoko)
    * [Nokia 770 Internet Tablet](https://en.wikipedia.org/wiki/Nokia_770_Internet_Tablet)
    * [N900](https://en.wikipedia.org/wiki/Nokia_N900)
    * [Moblin](https://en.wikipedia.org/wiki/Moblin), [MeeGo](https://en.wikipedia.org/wiki/MeeGo), [Nokia N9](https://en.wikipedia.org/wiki/Nokia_N9), [Mer](https://en.wikipedia.org/wiki/MeeGo#Mer), [Nemo](https://en.wikipedia.org/wiki/MeeGo#Nemo_Mobile), [SailfishOS](https://sailfishos.org/), [Tizen](https://www.tizen.org/)
    * [WebOS](https://en.wikipedia.org/wiki/WebOS), [LuneOS](https://en.wikipedia.org/wiki/LuneOS)
    * [LiMo Foundation](https://en.wikipedia.org/wiki/LiMo_Foundation)
    * [Access Linux Platform](https://en.wikipedia.org/wiki/Access_Linux_Platform)
    * [Firefox OS](https://en.wikipedia.org/wiki/Firefox_OS)
    * Cyanogenmod → [LineageOS](https://www.lineageos.org/)
    * [Replicant](https://www.replicant.us/)
    * projetos de engenharia reversa de GPUs:
      * [lima](https://limadriver.org/)
      * [freedreno](https://github.com/freedreno-zz/freedreno)
      * [etnaviv](https://github.com/etnaviv)
    * Plasma Active → [Plasma Mobile](https://community.kde.org/Plasma/Mobile)
    * [LWN: Why the Vivaldi tablet never came to market](https://lwn.net/Articles/606100/)
    * [Ubuntu Touch](https://en.wikipedia.org/wiki/Ubuntu_Touch)
      * [ubports](https://ubports.com/)
    * [C.H.I.P e Pocket C.H.I.P](https://getchip.com/)
    * [Zerophone](https://www.crowdsupply.com/arsenijs/zerophone)
    * [postmarketOS](https://www.postmarketos.org/)
    * [Halium](https://halium.org/)
    * [Purism Librem 5](https://puri.sm/shop/librem-5/)
  * [Debian Mobile](https://wiki.debian.org/Mobile)
  * Dicas
    * [Software Freedom Day](http://www.softwarefreedomday.org/) (Cascardo)
    * [Libreplanet Brasil](http://libreplanetbr.org/) (Cascardo)
    * [IRPF-Livre](https://www.fsfla.org/ikiwiki/blogs/lxo/2017-04-07-IRPF-Livre-2017.pt.html), [declara](http://git.libreplanetbr.org/?p=declara.git;a=summary), [rnetclient](http://git.libreplanetbr.org/?p=rnetclient.git;a=summary) (Cascardo)
    * [Episódio 80 do Opencast sobre o FISL e o adiamento do FISL18](http://tecnologiaaberta.com.br/2017/08/opencast-80-fisl-18/) (Paulo)
    * [FTSL](http://ftsl.org.br/) - 27 a 29 setembro em Curitiba (Paulo)
    * [Rick & Morty](https://pt.wikipedia.org/wiki/Rick_and_Morty) (Terceiro)
short_description: |
  Conversamos com o convidado Thadeu Cascardo sobre por quê a gente quer poder
  rodar GNU/Linux nos dispositivos móveis, como isso pode ser feito hoje, quais
  desafios ainda existem, e o histórico de iniciativas nesta área.
---

Antonio Terceiro, Paulo Santana e Thadeu Cascardo discutem por quê queremos
poder rodar GNU/Linux nos nossos dispositivos móveis, como isso pode ser feito
hoje em dia, quais os desafios que vamos encontrar ao tentar fazer isso. Também
revisitamos uma longa lista de iniciativas relacionadas, até chegar ao
[crowdfundind do Purism Librem 5](https://puri.sm/shop/librem-5/), um telefone
projetado para rodar GNU/Linux.

Edição: Thiago Mendonça

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

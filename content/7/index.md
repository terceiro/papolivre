---
kind: article
audio:
- filename: papolivre-7-eventos-de-software-livre.mp3
  filesize: 104595456
- filename: papolivre-7-eventos-de-software-livre.ogg
  filesize: 102339095
created_at: "2017-09-07"
title: "#7 - Eventos de Software Livre"
duration: 5551
links: |
  * [Campus Party Bahia](http://brasil.campus-party.org/bahia/)
  * [Debian Day](https://wiki.debian.org/DebianDay)
  * [0xe Hacker Club](http://www.oxehc.com.br/)
  * [Debconf](https://debconf17.debconf.org/) (veja também o [episódio #6](/6/)).
  * [Campus Party](http://brasil.campus-party.org/)
  * [Executiva Nacional de Estudantes de Computação](https://enec.org.br/)
  * [FISL](http://fisl.org.br/)
  * [ENECOMP](https://enec.org.br/EneComp)
  * [I Festival Software Livre da Bahia](http://wiki.dcc.ufba.br/Installfest)
    (então chamado "Festival GNU/Linux de Salvador")
  * [FISL18 adiado para 2018](http://softwarelivre.org/portal/noticias/fisl18-esta-adiado-para-julho-de-2018)
  * [Latinoware](http://latinoware.org/)
  * [Fórum Goiano de Software Livre](http://2017.fgsl.net/)
  * [FTSL](http://ftsl.org.br/)
  * [RubyConf Brasil](http://rubyconf.com.br/
  * [PHP Conference Brasil](http://phpconference.com.br/)
  * [Wordcamp Brasil](https://2017.portoalegre.wordcamp.org/)
  * [Python Brasil](http://2017.pythonbrasil.org.br/)
  * [Heresies in Free Software - what do the next 20 years look like?](https://debconf17.debconf.org/talks/177/), palestra de Mathew Garret na Debconf17.
  * [Associação Software Livre.Org](http://asl.org.br/)
  * [FLISOL](https://flisol.info/)
  * [Software Freedom Day](https://www.softwarefreedomday.org/)
  * [MiniDebconf 2017](http://br2017.mini.debconf.org/)
    * [Debate: Eventos Debian Brasil em 2017](http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/07_Debate_Eventos_Debian_Brasil_em_2017.webm) (webm, 507MB)
  * [Poticon](http://www.potilivre.org/poticon/)
  * [Café com Software Livre](https://blusol.org/csl2016/)
  * [Encontro Nordestino de Software](https://pt.wikipedia.org/wiki/Encontro_Nordestino_de_Software_Livre)
  * [Eventos do Texto Livre](http://textolivre.org/site/eventos-do-texto-livre/)
  * [appear.in](https://appear.in/)
  * [Jitsi](https://meet.jit.si/)
  * [OBS Studio](https://obsproject.com/)
  * Dicas
    * [Audacity](http://www.audacityteam.org/) (Terceiro)
    * [redEclipse](https://redeclipse.net/) (Thiago)
    * [SuperTuxKart](https://supertuxkart.net/) (Thiago)
    * [0.A.D](https://play0ad.com/) (Thiago)
    * [OpenRA](http://www.openra.net/) (Thiago)
    * [TeeWorlds](https://www.teeworlds.com/) (Thiago)
    * [Latinoware 2017 - 18 a 20/10](http://latinoware.org/) (Paulo)
    * [Campus Party](http://brasil.campus-party.org/), próximas edições (Paulo)
      * Pato Branco (14 e 15/10/2017)
      * Minas gerais (01 a 05/11/2017)
      * Brasil (30/01 a 04/02/2018)
    * [phls.com.br](http://phls.com.br/) (Paulo)
    * [timeline.softwarelivre.org](http://timeline.softwarelivre.org/) (Paulo)
    * Visite [Cuiabá](https://pt.wikivoyage.org/wiki/Cuiab%C3%A1) (Nobres e Bom
      Jardim) e [Maceió](https://pt.wikivoyage.org/wiki/Macei%C3%B3)
short_description: |
  Eventos de Software Livre, FISL18 adiado para 2018, questões sobre patrocínio
  de empresas a eventos de software livre (incluindo uma certa obsessão com
  patrocínios da Microsoft), e dicas para quem quer começar a organizar eventos
  de software livre.
  
---

Antonio Terceiro, Thiago Mendonça e Paulo Santana discutem eventos de Software
Livre, o adiamento do FISL18 para 2018, questões sobre patrocínio de empresas a
eventos de software livre (incluindo uma certa obsessão com patrocínios da
Microsoft), e dão dicas para quem quer começar a organizar eventos de software
livre.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

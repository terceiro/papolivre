---
kind: article
created_at: "2020-07-29"
title: "#20 - COVID-19 × Eventos de Software Livre"
duration: 4193
audio:
- filename: papolivre-20-covid-19-x-eventos-de-software-livre.ogg
  filesize: 49772836
- filename: papolivre-20-covid-19-x-eventos-de-software-livre.mp3
  filesize: 67554790
links: |
  - [Versão em Vídeo no Youtube](https://youtu.be/h1lj9NwuLkE)
  - Sugestões de canais no YouTube:
    - [LinuxTips](https://www.youtube.com/linuxtips)
    - [Foca Linux](https://www.youtube.com/channel/UCpOGZjB-b6UIiFBpHRXBX8g)
    - [PHPPR](https://www.youtube.com/channel/UCOg6FA5L4kK-eo_IUaHuaBw)
    - [Paulo Kretcheu](https://www.youtube.com/channel/UCQTTe8puVKqurziI6Do-H-Q)
    - [Eriberto Mota](https://www.youtube.com/channel/UCAXxhRve_scJJdJI4cwRSLg)
    - [Debxp](https://www.youtube.com/channel/UC8EGrwe_DXSzrCQclf_pv9g)
    - [Comunidade Fedora Brasil](https://www.youtube.com/channel/UCcAjDkUhcvjSJUWPL-4blMQ)
    - [Debian Brasil](https://www.youtube.com/debianbrasil)
short_description: |
  O Papo Livre está de volta! Neste episódio de retorno, Antonio Terceiro,
  Paulo Santana e Thiago Mendonça discutem como a pandemia de COVID-19 afetou
  os eventos de software livre.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça discutem os eventos de
Software Livre de 2020 durante a pandemia de COVID-19 neste episódio especial
que marca o retorno do Papo Livre para mais uma temporada!

Edição: Thiago Mendonça

Trilha sonora: [Snake Alley Blues](https://www.jamendo.com/track/804916/snake-alley-blues),
por [santa klaus - klausbluetner1](https://www.jamendo.com/artist/370281/santa-klaus-klausbluetner1).
!icença: Creative Commons Atribuição (CC-BY).

---
kind: article
audio:
- filename: papolivre-6-debconf-e-contribuindo-com-o-debian.mp3
  filesize: 70311936
- filename: papolivre-6-debconf-e-contribuindo-com-o-debian.ogg
  filesize: 65616182
created_at: "2017-08-22"
title: "#6 - Debconf e contribuindo com o Debian"
duration: 3838
links: |
  * Apresentações
    * [Debian Ruby team](https://wiki.debian.org/Teams/Ruby)
    * [Debian Perl Group](https://wiki.debian.org/Teams/DebianPerlGroup)
    * [Debian LTS](https://wiki.debian.org/LTS)
    * [Debian Security Tools Packaging Team (pkg-security)](https://wiki.debian.org/Teams/pkg-security)
    * [Kali linux](https://www.kali.org/)
    * [pkg-netfilter](https://wiki.debian.org/Teams/pkg-netfilter)
  * Debconf
    * [Debconf17, Montreal](https://debconf17.debconf.org/)
    * [Cheese and Wine party](https://wiki.debconf.org/wiki/DebConf17/CheeseWineBoF)
    * [Programação](https://debconf17.debconf.org/schedule/) (inclui links para vídeos das palestras gravadas)
  * Contribuindo com o Debian
    * [Debian New Maintainers Guide](https://www.debian.org/doc/manuals/maint-guide/)
    * [Video-aulas de empacotamento do eriberto](http://eriberto.pro.br/wiki/index.php?title=Algumas_coisas_sobre_Debian...)
    * [Submissão do Kanashiro para o Summer of Code 2014](https://wiki.debian.org/SummerOfCode2014/StudentApplications/LucasKanashiro)
    * [Submissão do Kanashiro para o Summer of Code 2015](https://wiki.debian.org/SummerOfCode2015/StudentApplications/LucasKanashiro)
    * ["Consentually Doing Things Together?" by Enrico Zini](https://debconf17.debconf.org/talks/92/)
  * Programas de inclusão
    * [Google Summer of Code](https://summerofcode.withgoogle.com/)
    * [Outreachy](https://www.gnome.org/outreachy/)
  * Dicas:
    * [Root beer](https://en.wikipedia.org/wiki/Root_beer) (Terceiro)
    * [Guardians of the Galaxy Vol. 2](http://www.imdb.com/title/tt3896198/) (Terceiro)
    * [htop](http://hisham.hm/htop/) (Terceiro)
    * [uMatrix](https://github.com/gorhill/uMatrix) (Samuel)
    * [Everything is a Remix](http://www.everythingisaremix.info/) (Samuel)
    * [MTR](http://www.bitwizard.nl/mtr/) (Samuel)
    * [Bento box](https://en.wikipedia.org/wiki/Bento) (Kanashiro)
    * [LXD](https://linuxcontainers.org/lxd/) (Kanashiro)
    * [tmate](https://tmate.io/) (Kanashiro)
short_description: |
  Neste episódio improvisado, Antonio Terceiro conversa com Lucas Kanashiro e
  Samuel Henrique sobre a Debconf17, e como contribuir com o projeto Debian.
---

Este episódio foi gravado diretamente de Montreal no Canadá, durante a
Debconf17. A Debconf é a conferência anual da comunidade Debian. Antonio
Terceiro recebe dois convidados especiais, Lucas Kanashiro (Debian Developer) e
Samuel Henrique (Debian Maintainer). Assuntos discutidos incluem impressões
sobre a Debconf, como eles começaram a contribuir com o Debian, e programas de
inclusão de novas pessoas em projetos de software livre, como Summer of Code e
Outreachy.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/), por [zero-project](https://zero-project.gr/). Licença Creative Commons Attribution (CC-BY).

---
kind: article
audio:
- filename: papolivre-18-historia-do-software-livre.mp3
  filesize: 87529520
- filename: papolivre-18-historia-do-software-livre.ogg
  filesize: 85888754
created_at: "2018-04-17"
title: "#18 - História do Software Livre"
duration: 4996
links: |
  * [Aracele Torres](https://aralito.wordpress.com/)
  * [Papo Livre #16 (KDE - Filipe Saraiva)](../16/)
  * [Universidade Federal do Piauí (UFPI)](http://ufpi.br/)
  * [Diretório Central dos Estudantes da UFPI](https://www.dceufpi.com/)
  * [LaKademy](https://lakademy.kde.org/)
  * [Akademy](https://akademy.kde.org/)
  * [A tecnoutopia do software livre: uma história do projeto técnico e político do GNU](http://www.teses.usp.br/teses/disponiveis/8/8138/tde-31032014-111738/pt-br.php); dissertação de mestrado da Aracele (2014)
  * [A história do GNU: 30 anos de criação do software livre](http://videos.softwarelivre.org/forum-internacional-software-livre-fisl-2014/a-historia-do-gnu-30-anos-de-criacao-do-software-livre.html); palestra no FISL 15 (2014)
  * [30 anos de software livre: uma análise histórica](http://videos.softwarelivre.org/forum-internacional-software-livre-fisl-2016/30-anos-de-software-livre-uma-analise-historica.html); palestra no FISL 17 (2016)
  * Dicas
    * [Coding Freedom: The Ethics and Aesthetics of Hacking](https://press.princeton.edu/titles/9883.html), Gabriella Colleman (Aracele)
    * [Free as in Freedom: Richard Stallman's Crusade for Free Software](https://sagitter.fedorapeople.org/faif-2.0.pdf), Sam Williams (Aracele)
    * [Two Bits: The Cultural Significance of Free Software](https://www.dukeupress.edu/Two-Bits/), Christophre Kelty (Aracele)
    * [Hackers: Heroes of the Computer Revolution](http://shop.oreilly.com/product/0636920010227.do), Steven Levy (Aracele)
    * [Novo gás no time de tradução do Debian](https://wiki.debian.org/Brasil/Traduzir) (Paulo)
    * [Atualização da timeline dos eventos de 2017 de Software Livre e Código Aberto no Brasil](http://timeline.softwarelivre.org) (Paulo)
    * [Anticast #267: Historiografia](http://anticast.com.br/2016/12/anticast/anticast-267-a-historiografia/) (Terceiro)
    * [Pantera Negra (2018)](https://www.imdb.com/title/tt1825683/) (Terceiro)
short_description: |
  Aracele Torres nos conta do seu trabalho de mestrado sobre a história do
  software livre: as primeiras iniciativas de RMS, a criação da GPL, o
  surgimento da FSF e do movimento open source, e nos fala sobre importância de
  entender o nosso contexto histórico.
---

Antonio Terceiro e Paulo Santana conversam com Aracele Torres sobre história do
software livre. Em seu trabalho de mestrando, Aracele estudou o processo de
surgimento do movimento do software livre, em especial a jornada de Richard M.
Stallman desde os tempos no laboratório da Inteligênia Artificial do MIT até a
criação do projeto GNU e a fundação da Free Software Foundation. Passamos
também pelo surgimento do movimento open source e da Open Source Initiative
como uma "alternativa" ao software livre, e pela importância de entendermos o
contexto onde ambos os movimentos nasceram para entender o nosso context atual.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

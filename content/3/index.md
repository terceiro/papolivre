---
kind: article
audio:
- filename: papolivre-3-software-livre-e-codigo-aberto.mp3
  filesize: 74124191
- filename: papolivre-3-software-livre-e-codigo-aberto.ogg
  filesize: 57007246
created_at: "2017-07-04"
title: "#3 - Software Livre e Código Aberto"
duration: 5397.365238
links: |
  * [Projeto GNU](https://www.gnu.org/)
  * [Free Software Foundation](https://www.fsf.org/)
  * [Definição de Software Livre](https://www.gnu.org/philosophy/free-sw.html)
    * [tradução em português brasileiro](https://www.gnu.org/philosophy/free-sw.pt-br.html)
  * [Lista de Licenças de Software Livre](https://www.gnu.org/licenses/license-list.html)
  * [Open Source Initiative](https://opensource.org/)
  * [Definição de Código Aberto](https://opensource.org/osd)
  * [Debian Free Software Guidelines](https://www.debian.org/social_contract#guidelines)
  * [Lista de Licenças de Código Aberto](https://opensource.org/licenses)
  * [Copyleft: Idealismo Pragmático](https://www.gnu.org/philosophy/pragmatic.html) (original)
    * [tradução em português brasileiro](https://www.gnu.org/philosophy/pragmatic.pt-br.html)
  * [Por que o Código Aberto não compartilha dos objetivos do Software Livre](https://www.gnu.org/philosophy/open-source-misses-the-point.html) (original)
    * [tradução em português brasileiro](https://www.gnu.org/philosophy/open-source-misses-the-point.pt-br.html)
  * [Linux Foundation](https://www.linuxfoundation.org/)
short_description: |
  Discutimos as definições de software livre e código aberto, e os fatos e
  mitos com relação à diferença entre este dois conceitos.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça discutem os conceitos de
software livre e código aberto, qual é de fato a diferença entre os dois, e
quais os mitos que são repetidos por aí sobre essa diferença.  Acabamos também
discutindo as hostilidades praticadas por pequenos grupos que se identificam
com um dos lados, se é que ver essa questão em termos de "lados" faz algum
sentido.

Edição: Thiago Mendonça.

Trilha sonora: The crossroads of change, The preparation, Path to innocence, The light of truth, Metamorphosis, do [álbum Metamorphosis](https://zero-project.gr/music/albums/metamorphosis/), por ["zero-project"](https://zero-project.gr/); e [Slow motion](https://zero-project.gr/music/tracks/slow-motion/), também por "zero-project". Distribuídas sob a licença [Creative Commons Attribution 3.0/4.0 Unported License](http://creativecommons.org/licenses/by/4.0/).

---
kind: article
audio:
- filename: papolivre-4-redes-sociais-livres.mp3
  filesize: 92020736
- filename: papolivre-4-redes-sociais-livres.ogg
  filesize: 71112902
created_at: "2017-07-19"
title: "#4 - Redes sociais livres"
duration: 4354
links: |
  * [GNU Social](https://gnu.io/social/)
    * [OStatus](https://en.wikipedia.org/wiki/OStatus)
    * [identi.ca](https://identi.ca/)
    * [pump.io](http://pump.io/)
    * [quitter.se](https://quitter.se/)
  * [Mastodon](https://mastodon.social/)
    * [Mastodon is dead in the water](https://hackernoon.com/mastodon-is-dead-in-the-water-888c10e8abb1)
  * [diaspora*](https://diasporafoundation.org/)
    * [diaspora.softwarelivre.org](https://diaspora.softwarelivre.org/)
  * [Hubzilla](https://hubzilla.org/)
    * [http://friendi.ca/](friendica)
  * [Noosfero](http://noosfero.org/)
  * [How did the world ever work without Facebook?](https://danielpocock.com/how-did-the-world-ever-work-without-facebook)
  * Nós nas redes socias livres:
    * Paulo
      * [diaspora*](https://diaspora.softwarelivre.org/u/phls)
      * [GNU Social](https://quitter.se/phls)
      * [pump.io](https://identi.ca/phls00)
      * [blogoosfero (Noosfero)](http://blogoosfero.cc/phls)
      * [softwarelivre.org (Noosfero)](http://phls.com.br/)
      * [Hubzilla](https://hub.vilarejo.pro.br/channel/phls)
    * Thiago
      * [diaspora*](https://diaspora.softwarelivre.org/u/mendonca)
      * [GNU Social](https://quitter.se/tfaria1991)
      * [softwarelivre.org (Noosfero)](http://softwarelivre.org/tmendonca)
    * Terceiro
      * [softwarelivre.org (Noosfero)](https://softwarelivre.org/terceiro)
short_description: |
  Os problemas com redes sociais proprietárias/centralizadas, e
  algumas das principais alternativas livres: GNU Social, Mastodon, diaspora*,
  Hubzilla, e Noosfero.
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça discutem quais são os
problemas com redes sociais proprietárias/centralizadas, e apresentam algumas
das principais alternativas livres: GNU Social, Mastodon, diaspora\*, Hubzilla,
e Noosfero.

Edição: Thiago Mendonça

Trilha sonora: The crossroads of change, The preparation, Path to innocence, The light of truth, Metamorphosis, do [álbum Metamorphosis](https://zero-project.gr/music/albums/metamorphosis/), por ["zero-project"](https://zero-project.gr/); e [Slow motion](https://zero-project.gr/music/tracks/slow-motion/), também por "zero-project". Distribuídas sob a licença [Creative Commons Attribution 3.0/4.0 Unported License](http://creativecommons.org/licenses/by/4.0/).

---
kind: article
audio:
- filename: papolivre-17-fedora.mp3
  filesize: 86254012
- filename: papolivre-17-fedora.ogg
  filesize: 65799232
created_at: "2018-03-27"
title: "#17 - Fedora"
duration: 4017
links: |
  * [Athos Ribeiro](https://www.ime.usp.br/~athoscr/)
  * [Projeto Fedora](https://getfedora.org/)
  * [Red Hat Enterprise Linux](https://pt.wikipedia.org/wiki/Red_Hat_Enterprise_Linux)
  * [CentOS](https://www.centos.org/)
  * [Fundamentos e Missão do projeto Fedora](https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html)
  * [Comitê de Engenharia do Fedora](https://docs.fedoraproject.org/fedora-project/subprojects/fesco.html)
  * [Conselho do Fedora](https://docs.fedoraproject.org/fedora-project/council/charter.html)
  * [Comitê de embaixadores, hoje "comitê de mindshare"](https://docs.fedoraproject.org/fedora-project/subprojects/mindshare.html)
  * [Fedora Project Leader](https://docs.fedoraproject.org/fedora-project/council/fpl.html)
  * [Proven Packagers](https://fedoraproject.org/wiki/Provenpackager_policy)
  * [Fedora rawhide](https://fedoraproject.org/wiki/Releases/Rawhide)
  * [Fedora Modularity](https://docs.pagure.org/modularity/)
  * Artigos no Linux Weekly News sobre o Fedora Modularity:
    * [Modularizing Fedora](https://lwn.net/Articles/680278/) (2016)
    * [Fedora's Boltron preview](https://lwn.net/Articles/732316/) (2017)
    * [A Modularity rethink for Fedora](https://lwn.net/Articles/742497/) (2018)
  * [Flock To Fedora: Fedora Contributor Conference](https://flocktofedora.org/)
  * [FUDCon: Fedora Users and Developers Conference](https://fedoraproject.org/wiki/FUDCon)
  * [Fedora Packaging Guidelines](https://fedoraproject.org/wiki/Packaging:Guidelines)
  * [Time de Tradução do Fedora para português brasileiro](https://fedoraproject.org/wiki/L10N_Brazilian_Portuguese_Team)
  * [Time de Embaixadores](https://fedoraproject.org/wiki/Ambassadors)
  * [Infraestrutura do projeto Fedora](https://fedoraproject.org/wiki/Infrastructure)
  * [Fedora Diversity](https://fedoraproject.org/wiki/Diversity)
  * Dicas
    * [Fedora: pkgwat](https://pypi.python.org/pypi/pkgwat.cli) (Athos)
    * análise estática de código fonte como ponto de entrada em projetos de software livre (Athos)
    * [Doações para a MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/donate/) (Paulo)
    * [Texto sobre ajudar com tradução](http://phls.com.br/blog/voce-pode-ajudar-um-projeto-de-software-livre-fazendo-traducoes) (Paulo)
    * [Sapiens - Uma Breve História da Humanidade](https://pt.wikipedia.org/wiki/Sapiens_%E2%80%93_Uma_Breve_Hist%C3%B3ria_da_Humanidade), de Yuval Harari (Paulo)
    * [Flowing Data](https://flowingdata.com/) (Terceiro)
    * [Tocandira](https://www.tocandira.com.br/) (Terceiro)
short_description: |
  Conversamos com Athos Ribeiro, colaborador do projeto Fedora, sobre o
  projeto, sua relação com RHEL e CentOS, aspectos da comunidade, como
  contribuir, e outros assuntos.
---

Paulo Santana e Antonio Terceiro conversam com Athos Ribeiro sobre o projeto
Fedora. A conversa aborda conceitos básicos sobre o projeto (fundamentos,
missão, organização intenna), a relação do Fedora com o Red Hat Enterprise
Linux e CentOS, eventos da comunidade Fedora, comunidade Fedora no Brasil, e
também dicas pra quem quer começar a contribuir com o projeto.

Edição: Thiago Mendonça

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

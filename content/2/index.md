---
kind: article
audio:
- filename: papolivre-2-debian-9-stretch.mp3
  filesize: 66860791
- filename: papolivre-2-debian-9-stretch.ogg
  filesize: 41335694
created_at: "2017-06-19"
title: "#2 - Debian 9 (stretch)"
duration: 3069.910181
links: |
  * [Site oficial do Debian](https://www.debian.org/)
  * [Debian 9: Anúncio oficial](https://www.debian.org/News/2017/20170617)
  * [Time de release do Debian](https://release.debian.org/)
  * [Time de segurança do Debian](https://www.debian.org/security/)
  * [Debian LTS](https://wiki.debian.org/LTS/)
  * [Dedicatória ao Ian Murdock](http://ftp.debian.org/debian/doc/dedication/dedication-9.0.txt)
    * [tradução para português do Brasil](http://ftp.debian.org/debian/doc/dedication/dedication-9.0.pt_BR.txt)
  * [mariadb](https://mariadb.org/)
  * [Virtualbox](https://www.virtualbox.org/)
  * [BIOS de VMs do Virtualbox depender de um compilador não livre](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=691148)
  * [#815006 Renaming Iceweasel to Firefox](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=815006)
    * [problema original com a política de marcas da Mozilla (Debian bug #354622)](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=354622)
  * [Wikipedia: Mozilla software rebranded by Debian](https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian) (Iceweasel, Icedove, etc)
  * [Weasel](https://en.wikipedia.org/wiki/Weasel) ~= [Doninha](https://pt.wikipedia.org/wiki/Mustela)
  * [Reprodudible Builds](https://reproducible-builds.org/)
  * [X não roda mais como root por padrão](https://www.debian.org/releases/stretch/amd64/release-notes/ch-whats-new.en.html#x-no-longer-requires-root)
  * [Wayland](https://wayland.freedesktop.org/)
  * [GnuPG](https://gnupg.org/)
  * [Campanha de arrecadação do GnuPG](https://gnupg.org/donate/index.html)
  * [Pacotes de debug automáticos](https://wiki.debian.org/AutomaticDebugPackages)
  * [UEFI - Unified Extensible Firmware Interface](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface)
  * [Notas de lançamento do Stretch amd64)](https://www.debian.org/releases/stretch/amd64/release-notes/)
  * [CDs, DVDs, BDs](https://www.debian.org/releases/stretch/amd64/release-notes/ch-whats-new.en.html#cd)
  * [melhorias no apt e no layout do repositório](https://www.debian.org/releases/stretch/amd64/release-notes/ch-whats-new.en.html#apt-improvements)
  * [deb.debian.org](http://deb.debian.org/)
  * [Nomenclatura previsível para interfaces de rede](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)
  * [systemd](https://www.freedesktop.org/wiki/Software/systemd/)
  * [Devuan](https://www.devuan.org/)
  * [Mudanças no instalador](https://www.debian.org/releases/stretch/amd64/release-notes/ch-installing.en.html)
  * [procedimento recomendado de atualização](https://www.debian.org/releases/stretch/amd64/release-notes/ch-upgrading.en.html)
  * [questões específicas durante a atualização para o Debian Stretch](https://www.debian.org/releases/stretch/amd64/release-notes/ch-information.en.html)
  * [Debian releases: stable, testing, unstable](https://wiki.debian.org/DebianReleases)
  * "Vivendo perigosamente!? Usando Debian unstable no dia-a-dia" - Palestra do Terceiro na MiniDebconf 2017:
    * [video](http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/06_Vivendo_perigosamente_Usando_Debian_unstable_no_dia-a-dia.webm) - WebM, 367MB
    * [slides](http://br2017.mini.debconf.org/slides/unstable.pdf)
  * [virt-manager](https://virt-manager.org/)
  * [GNOME boxes](https://wiki.gnome.org/Apps/Boxes)
  * [Todas as Palestras da MiniDebconf Brasil 2017](http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/)
short_description: |
  Discutimos as principais novidades no Debian 9 (stretch) com base no anúncio
  oficial e nas notas de lançamento, e perguntas frequentes e curiosidades
  sobre o Debian em geral.
---

A mais nova versão estável do Debian, de número 9 e com codinome Stretch, foi
lançada no dia 17 de junho de 2017. Neste episódio, nós discutimos os destaques
da nova versão com base no anúncio oficial a nas notas de lançamento. Tmabém
discutimos algumas dúvidas comuns sobre o Debian, como a diferença entre
stable, testing e unstable, como atualizar o sistema, e outros.

Pauta principal: 4:26

Edição: Thiago Mendonça.

Trilha sonora: [Going Higher by Bensound](http://www.bensound.com/royalty-free-music/track/going-higher).
Licenciada sob a [Creative Commons Attribution-NoDerivatives 3.0](http://creativecommons.org/licenses/by-nd/3.0/legalcode).

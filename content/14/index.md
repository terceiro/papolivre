---
kind: article
audio:
- filename: papolivre-14-gobolinux.mp3
  filesize: 77923407
- filename: papolivre-14-gobolinux.ogg
  filesize: 62596299
created_at: "2017-12-26"
title: "#14 - GoboLinux"
duration: 5120
links: |
  * [Hisham H. Muhammad](http://hisham.hm/)
  * [Lucas C. Villa Real](http://lucasvr.gobolinux.org/)
  * [GoboLinux](https://gobolinux.org/)
  * [Compile: the GoboLinux compilation system](https://gobolinux.org/compile.html)
  * [GoboHide: surviving aside the legacy tree](https://gobolinux.org/doc/articles/gobohide.html)
  * [GoboLinux Recipes](https://gobolinux.org/recipes.html)
  * [GoboNet](https://github.com/gobolinux/GoboNet)
  * Dicas
    * [GDAL](http://www.gdal.org/) (Lucas)
    * [proj.4](http://proj4.org/) (Lucas)
    * [Inkscape](https://inkscape.org/en/) (Lucas)
    * [Itch: app store para jogos indie](https://itch.io) (Hisham)
    * [Lapis: web framework feito em MoonScript](http://leafo.net/lapis/) (Hisham)
    * [MoonScript: linguagem que compila para Lua ("CoffeeScript para Lua")](https://moonscript.org) (Hisham)
    * [RetroArch](http://www.retroarch.com) (Thiago)
    * [Fiozera](https://fiozera.com.br/) (Lucas)
    * [system76](https://system76.com/) (Terceiro)
    * [Fluxx](http://www.looneylabs.com/games/fluxx) (Terceiro)
short_description: |
  Conversamos com Hisham Muhammad e Lucas Villa Real sobre o GoboLinux, uma
  distribuição à frente do seu tempo. Discutimos as origens do projeto,
  inovações, evolução, estado atual, e muito mais.
---

Antonio Terceiro e Thiago Mendonça conversam com Hisham Muhammad e Lucas Villa
Real sobre o GoboLinux, uma distribuição criada no Brasil que introduziu várias
inovações em termos de gestão de pacotes e outros tópicos. Abordamos as origens
do projeto, sua evolução, diversos sub-projetos interessantes, o estado atual
  da comunidade e curiosidades.

Edição: Thiago Mendonça.

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

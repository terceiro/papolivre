---
kind: article
audio:
- filename: papolivre-16-kde.mp3
  filesize: 83044846
- filename: papolivre-16-kde.ogg
  filesize: 66027642
created_at: "2018-02-23"
title: "#16 - KDE"
duration: 4401
links: |
  * [Filipe Saraiva](http://filipesaraiva.info/)
  * [KDE](https://www.kde.org/)
  * [KmPlot](https://edu.kde.org/kmplot/)
  * [Cantor](https://edu.kde.org/cantor/)
  * [Trolltech (hoje chamada "The Qt Company")](https://en.wikipedia.org/wiki/The_Qt_Company)
  * [Qt](https://www.qt.io/)
  * [Plasma](https://www.kde.org/plasma-desktop)
  * [KDE Applications](https://www.kde.org/applications/)
  * [KDE Frameworks](https://api.kde.org/frameworks/)
  * [Plasma Mobile](https://www.plasma-mobile.org/)
  * [KDE Connect](https://community.kde.org/KDEConnect)
  * [Dolphin](https://www.kde.org/applications/system/dolphin/)
  * [Kate](http://kate-editor.org/)
  * [Krita](https://krita.org/)
  * [KDEnlive](https://kdenlive.org/)
  * [KDE Edu](https://edu.kde.org/)
  * [Instância de Phabricator do KDE](https://phabricator.kde.org/)
  * [Season of KDE](https://season.kde.org/)
  * [LaKademy](https://lakademy.kde.org/)
  * [KDE-Brasil](https://br.kde.org/)
  * [Linha do tempo 20 anos de KDE](https://timeline.kde.org/)
  * [Livro _20 Years of KDE: Past, Present and Future_](https://20years.kde.org/book/)
  * [KDE Community no Google Play Store](https://play.google.com/store/apps/dev?id=4758894585905287660&hl=pt)
  * Dicas
    * [5 dicas para quem está pensando em migrar para o Linux](https://web.archive.org/web/20180131181109/https://canaltech.com.br/linux/5-dicas-para-quem-esta-pensando-em-migrar-para-o-linux-107456/) (Thiago)
    * [Arcade Stick Sanwa](http://bit.ly/2DWP9DU) (Thiago)
    * ["CPBR11" no Youtube](https://www.youtube.com/results?search_query=CPBR11) (Paulo)
    * [Debconf19 em Curitiba](https://wiki.debconf.org/wiki/DebConf19) (Paulo)
    * Peça a volta do palco de Software Livre no Twitter: #CPBR11 #SoftwareLivre @CampusPartyBRA (Paulo)
    * [Qt](http://qt.io/) (Filipe)
    * [QtCon Brasil](http://br.qtcon.org/) (Filipe)
    * [Google Summer of Code](https://summerofcode.withgoogle.com/) (Terceiro)
    * [Brooklyn Nine-Nine](http://www.imdb.com/title/tt2467372/) (Terceiro)
short_description: |
  Conversamos com Filipe Saraiva, professor universitário e contribuidor do
  KDE, sobre as origens da comunidade KDE, sua organização, projetos, como
  contribuir, e muito mais.
---

Antonio Terceiro, Thiago Mendonça e Paulo Santana conversam com Filipe Saraiva
sobre a comunidade KDE. Abordamos as origens da comunidade, os diversos
projetos desenvolvidos por ela, ferramentas utilizadas no desenvolvimento, a
comunidade brasileira e seus eventos, e a disponibilidade de software KDE para
diversos dispositivos.

Edição: Thiago Mendonça

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

---
kind: article
audio:
- filename: papolivre-9-linux-kernel-hacking.mp3
  filesize: 89740640
- filename: papolivre-9-linux-kernel-hacking.ogg
  filesize: 87415363
created_at: "2017-10-07"
title: "#9 - Linux Kernel Hacking"
duration: 4850
links: |
  * [The Linux Kernel Archives](https://www.kernel.org/)
  * [Collabora](https://www.collabora.com/)
  * [Userspace](https://en.wikipedia.org/wiki/User_space)
  * [BusyBox](https://busybox.net/)
  * [Linux com LLVM](http://llvm.linuxfoundation.org/index.php/Main_Page)
  * [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/)
  * [Outreachy](https://www.outreachy.org/)
  * [video4linux](https://en.wikipedia.org/wiki/Video4Linux)
  * [IBM Linux Techonology Center](https://en.wikipedia.org/wiki/Linux_Technology_Center)
  * [DRM — Direct Rendering Manager](https://en.wikipedia.org/wiki/Direct_Rendering_Manager)
  * [git.kernel.org](https://git.kernel.org/)
  * [Linux Kernel Newbies](https://kernelnewbies.org/)
  * [Linux mainline](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/) (árvore do Linus Torvalds)
  * [Linux Kernel in a Nutshell](http://www.kroah.com/lkn/), por Greg Kroah-Hartman
  * [printk](https://en.wikipedia.org/wiki/Printk)
  * [dmesg](https://en.wikipedia.org/wiki/Dmesg)
  * [A guide to the Kernel Development Process](https://www.kernel.org/doc/html/latest/process/development-process.html)
  * [Linux stable tree](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/)
  * [Working with linux-next](https://www.kernel.org/doc/man-pages/linux-next.html)
  * [Linux Weekly News (lwn.net)](https://www.lwn.net/)
  * [Why Github can't host the Linux Kernel Community](http://blog.ffwll.ch/2017/08/github-why-cant-host-the-kernel.html)
  * [Linux Kernel Summit](http://events.linuxfoundation.org/events/linux-kernel-summit)
  * [Kernel Recipes](https://kernel-recipes.org/)
  * [Open Source Summit North America](http://events.linuxfoundation.org/events/open-source-summit-north-america)
  * [Linux Developer Conference Brazil](http://linuxdev-br.net/)
  * Dicas
    * [Cockatrice](https://cockatrice.github.io) (Thiago)
    * [XMage](http://xmage.de) (Thiago)
    * [Nightmare Masterclass](https://www.youtube.com/channel/UCsoZWD7Uagg2yv3YcQ3zqBA) (Thiago)
    * [Debian & Outreachy](https://debconf17.debconf.org/talks/86/) — palestra da Karen Sandler (Paulo)
    * [Mulheres, Tecnologia e Oportunidades](http://mulheres.eti.br/) (Paulo)
    * [Como foi o Debian Day 2017 no Brasil](http://debianbrasil.org.br/blog/como-foi-o-debian-day-2017-no-brasil) (Paulo)
    * [Linux Device Drivers](https://lwn.net/Kernel/LDD3/) (Krisman)
    * [Palestra da Kernel Recipes: Speed up your kernel development cycle with QEMU](https://kernel-recipes.org/en/2015/talks/speed-up-your-kernel-development-cycle-with-qemu/) (Krisman)
    * [Linux GPU Driver Developer’s Guide » TODO list](https://www.kernel.org/doc/html/latest/gpu/todo.html) (Krisman)
    * [Debian on a Smartphone](https://cascardo.eti.br/blog/Debian_on_a_Smartphone/) (Krisman)
    * [Muzei — Live wallpaper of famous art](http://muzei.co/) (Krisman)
      * use a [versão do f-droid](https://f-droid.org/packages/net.nurik.roman.muzei/)!
    * [#kernelnewbies na oftc IRC](https://kernelnewbies.org/IRC); fale com `koike` :-) (Helen)
    * [Tutorial: Outreachy First Patch](https://kernelnewbies.org/Outreachyfirstpatch) (Helen)
    * Frequentar conferências.  (Helen)
      * [Programa de bolsas da LF](https://events.linuxfoundation.org/travel-request/request)
      * [Diversity Scholarship da LF](http://events.linuxfoundation.org/events/archive/2016/openiot-summit/attend/diversity-scholarship)
    * [Nanoc](https://nanoc.ws) (Terceiro)
    * Fazer exercício físico (Terceiro)
short_description: |
  Helen Koike a Gabriel Krisman nos contam como começaram a participar do
  desenvolvimento do Linux (o kernel) e dão dicas pra quem gostaria de começar
  a participar. E afinal, a comunidade do Linux é realmente tão hostil quanto
  parece?
---

Antonio Terceiro, Paulo Santana e Thiago Mendonça recebem os convidados Helen
Koike e Gabriel Krisman para discutir desenvolvimento do Linux (o kernel).
Eles nos contam como começaram a participar do desenvolvimento do Linux,
ensinam noções básicas do que é um kernel e o que ele faz, e dão dicas pra quem
gostaria de começar a desenvolver para o Linux. Ao final fazemos uma discussão
sobre a percepção que se tem de que a comunidade do Linux é hostil.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

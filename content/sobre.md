<img src="/images/GNU-meditate-papolivre-small.png" style='float: right' class='hidden-xs'/>

## Sobre

O Papo Livre é um podcast que tem como missão discutir Software Livre em todos os
seus aspectos. Desenvolvimento, comunicação, filosofia, comunidade, e tudo
mais, em conversas informais com opiniões claras.

### Equipe

[Antonio Terceiro](http://softwarelivre.org/terceiro) é baiano de Salvador, e
mora em Curitiba. Engenheiro de Software da Linaro Limited. Desenvolvedor de
Software Livre, membro do projeto Debian.

[Paulo Santana](http://phls.com.br/) mora em Curitiba e trabalha com
administração de sistemas GNU/Linux, organiza uma série de eventos de software
livre. Colabora com o projeto Debian, atualmente como Debian Maintainer.

[Thiago Mendonça](http://acesso.me/) mora num buraco no estado do Rio de
Janeiro chamado Conceição de Macabu e gerencia o departamento de TI de uma
fábrica de móveis. Participa de várias comunidades de software livre e tem
participado de diversos eventos de software livre

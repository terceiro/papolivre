---
kind: article
created_at: "2020-09-11"
title: "#21 - Outreachy e Testes de Usabilidade"
audio:
- filename: papolivre-21-outreachy-e-testes-de-usabilidade.mp3
  filesize: 69958285
- filename: papolivre-21-outreachy-e-testes-de-usabilidade.ogg
  filesize: 54326187
duration: 4729
links: |
  - Clarisa na GUADEC: [ligthning talk](https://youtu.be/8jBrQFBjmlw?t=280); [palestra](https://www.youtube.com/watch?v=YCYxjY80skU)
  - [Relatório final do Outreachy](https://lborgesclarissa.wordpress.com/2019/03/04/final-internship-report/)
  - [Relato sobre dificuldades no Outreachy](https://lborgesclarissa.wordpress.com/2018/12/25/week3-outreachy-post-everybody-struggles/)
  - [Anúncio do Free Software Awards](https://www.fsf.org/news/lets-encrypt-jim-meyering-and-clarissa-lima-borges-receive-fsfs-2019-free-software-awards)
  - [Entrega do Free Software Awards](https://media.libreplanet.org/u/libreplanet/m/fsf-award-ceremony/) (a partir de 7m30s)
  - [BOSS - Big Open Source Sister](https://github.com/BOSS-BigOpenSourceSister)
short_description: |
  Clarissa Borges fala com a equipe do Papo Livre sobre sua participação no Outreachy com testes de usabilidade no GNOME, e sobre o prêmio que ela recebeu no Free Software Awards da FSF.
---

Clarissa Borges é estudante de Engenharia de Software na Universidade de
Brasília, e participou to programa Outreachy em 2018, trabalhando com testes de
usabilidade no GNOME. Neste episódio ela conversa com a equipe do Papo Livre
sobre a sua participação no programa, as suas angústias e alegrias de começar a
participar da comunidade, e nos ensina sobre testes de usabilidade. Ela também
nos fala sobre a experiência de receber o Award for Outstanding New Free
Software Contributor da Free Software Foundation, e sobre suas iniciativas de
incentivar mais pessoas a participar de projetos de software livre.


Edição: Thiago Mendonça

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/),
por [zero-project](https://zero-project.gr/). Licença Creative Commons
Attribution (CC-BY).

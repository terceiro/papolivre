---
title: Instruções para gravação
---

## 0) Instalar o mumble

O mumble é a aplicação de áudio-chat que usamos pra gravar o podcast. Muito
provavalmente a sua distribuição já tem um pacote dele, no Debian ele se chama
`mumble` mesmo.

### 0.1) Configuração do cliente Mumble

Ao abrir o cliente pela primeira vez haverá um assistente de configuração,
avance por ele (provavelmente a configuração padrão de input e output será
suficiente), deixe ativo o áudio posicional e de preferencia o atenuar
aplicativos enquanto outros falam.

As configurações de sensibilidade e latência são bem intuitivas, porém, nas
opções de detecção de voz, recomendamos a utilização do push do talk ("apertar
pra falar"), ele evita ruido desnecessário durante a conversa. O atalho de
teclado a ser utilizado fica a seu critério.

Nas configurações de qualidade de áudio, certifique-se de usar no máximo 64kps.

![Compressao: use 64kpbs no máximo](mumble-compressao.png)

## 1) Conectar ao servidor

Ao iniciar o mumble, selecione o servidor brasileiro "default" que o mumble
mostra no início ("Clanwarz Mumo: Brazil Pub").

![Conectar ao servidor "Clanwarz Mumo: Brazil Pub"](0-servidor.png)

## 2) Entrar no canal

Antes da gravação, vamos passar um nome de canal e uma senha. A senha deverá ser adicionada na lista de credenciais registradas em Servidor -> Credenciais de Acesso. Normalmente o
nome do canal vai ter alguma coisa a ver com o nome do podcast, com alguma
variação em função do humor de quem criar o canal nesse dia.

Depois de conectar ao servidor, procure o canal na lista, e dê um clique duplo
para entrar.

![Entrar no canal](1-canal.png)

## 3) Iniciar gravação

Todos habilitam a gravação local de audio do mumble (ícone de gravação, círculo
vermelho).

![Iniciar gravação](2-gravar.png)

Essa gravação é um backup local da gravação remota, e além disso em
geral a qualidade do áudio local é melhor do que aquela que os outros
participantes da gravação recebem. Usar os seguintes parâmetros:

- Formato da saída: .flac
- Caminho de destino: qualquer um, desde que seja um diretório que não seja
  apagado no próximo reboot. ;-)
- Nome de arquivo: `%user`
- Modo: Multicanal

![Parâmetros de gravação](3-config-gravar.png)

Clique em "Início". A janela da gravação vai ficar aberta permanentemente, e
normalmente não dá pra minimizar. Pra evitar parar a gravação acidentalmente,
mova a janela de gravação pra uma área de trabalho vazia, ou bem pro canto da
tela.

## 4) Durante a gravação

* Procure estar num lugar silencioso durante a gravação.
* De preferência, use um headset ou microfone + fones de ouvido. Usar o
  microfone embutido de um laptop produz resultados ruins.
* Verifique que o seu microfone não vai raspar na sua roupa ou no seu rosto,
  pra evitar ruídos estranhos enquanto você fala.
* Caso haja uma fonte externa de barulho (ex: motocicleta ou ambulância
  passando perto, cachorro latindo, vizinho tocando bateria), interrompa o que
  está falando, e retome a frase do início depois que o barulho terminar.
  Depois na edição a gente corta o barulho e a frase interrompida.
* É normal que a gente interrompa frases no meio para pensar, soltar um "éééé",
  "então...", gaguejar, etc. Não tem problema: simplesmente retome o
  raciocínio, que a gente corta depois na edição. Se você achar que a frase
  ficou muito truncada, é só dizer "vou repetir", e retomar a frase de novo,
  que a gente corta a anterior na edição.

## 5) Disponibilizar o seu áudio local

Após o final da gravação, recupere a janela de gravação do mumble e clique em
"Parar". Procure o diretório onde você gravou os áudio locais, localize o
arquivo chamado `Recorder.flac`.

_Nota_: o nome do arquivo é literalmente `Recorder.flac`; o Mumble sempre usa
`Recorder` como o nome de usuário de quem está fazendo a gravação.

Acesse [upload.papolivre.org](https://upload.papolivre.org/), entre a senha
(que vamos te passar em _off_), e faça o upload do `Recorder.flac`. Nos passe a
URL de download gerada pela ferramenta.

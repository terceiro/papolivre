---
kind: article
audio:
- filename: papolivre-5-computador-so-com-software-livre.mp3
  filesize: 96690176
- filename: papolivre-5-computador-so-com-software-livre.ogg
  filesize: 94377495
created_at: "2017-08-03"
title: "#5 - computador só com software livre?"
duration: 5118
links: |
  * [Drivers de dispositivo](https://pt.wikipedia.org/wiki/Driver_de_dispositivo)
  * [Bootloader](https://pt.wikipedia.org/wiki/Boot)
    * [GRUB](https://www.gnu.org/software/grub/)
    * [syslinux](http://www.syslinux.org/)
    * [lilo](http://lilo.alioth.debian.org/)
  * [BIOS](https://pt.wikipedia.org/wiki/BIOS)
    * [coreboot](https://www.coreboot.org/)
    * [libreboot](https://libreboot.org/)
  * [Microcódigo](https://en.wikipedia.org/wiki/Microcode)
    * [Bug no microcódigo da Intel](https://lists.debian.org/msgid-search/20170625121936.GA7714@khazad-dum.debian.net)
    * [intel-microcode](https://packages.debian.org/sid/intel-microcode)
    * [amd64-microcode](https://packages.debian.org/sid/amd64-microcode)
  * [Firmware](https://pt.wikipedia.org/wiki/Firmware)
  * [h-node](https://h-node.org/)
  * [Trisquel](https://trisquel.info/)
  * [nouveau - driver livre para GPUs Nvidia](https://nouveau.freedesktop.org/)
    * [tabela de funcionalidades implementadas por família de GPU](https://nouveau.freedesktop.org/wiki/FeatureMatrix/)
  * [Dell Inspiron 15 5000 (5666)](http://www.dell.com/br/p/inspiron-15-5566-laptop/pd?oc=cai5566u181003br&model_id=inspiron-15-5566-laptop)
    * [Dell Wireless 1707™ = Atheros AR9565](https://wikidevi.com/wiki/Dell_Wireless_1707_%28DW1707%29)
  * [Minifree](https://minifree.org/)
  * [ThinkPenguin](https://www.thinkpenguin.com/)
  * [EOMA68](https://www.crowdsupply.com/eoma68)
  * [Purism](https://puri.sm/)
    * [Roadmap: Road to FSF endorsement… and Beyond](https://puri.sm/learn/freedom-roadmap/)
  * [Lemote Yeeloong](https://en.wikipedia.org/wiki/Lemote#Netbook_computers)
  * [FLISOL](https://flisol.info/)
  * Dicas:
    * Paulo
      * [videos.softwarelivre.org](http://videos.softwarelivre.org/)
      * [Debian Day](https://wiki.debian.org/DebianDay)
      * [Software Freedom Day](http://www.softwarefreedomday.org/)
    * Terceiro
      * [Debconf](https://debconf.org/)
      * [MiniDebconf Brasil 2017](http://br2017.mini.debconf.org/)
        * [Vídeos das palestras](http://videos.softwarelivre.org/eventos/minidebconf-curitiba-2017.html)
    * Thiago
      * [Disroot](https://disroot.org/)
      * [nativefier](https://github.com/jiahaog/nativefier)
short_description: |
  Onde existe software num computador moderno? Existe computador que possa ser
  usado apenas com software livre? Como comprar um computador o mais amigável
  possível ao software livre?
---

Neste episódio conversamos sobre quais partes do computador, além do óbvio
(sistemas operacional e aplicativos), podem ter software. Trocamos dicas de
como comprar computadores o mais amigáveis possível ao software livre, e
discutimos iniciativas voltadas à produção de computadores mais livres que os
que se encontra normalmente.

Neste episódio também estreamos um quadro de dicas, onde nós damos indicações
de coisas interessantes, ligadas ou não a software livre.

Edição: Antonio Terceiro

Trilha sonora: [Slow motion](https://zero-project.gr/music/tracks/slow-motion/), por [zero-project](https://zero-project.gr/). Licença Creative Commons Attribution (CC-BY).
